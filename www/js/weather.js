var DEV = 'release';

function get_weather(location){
    var that = this;
    $.ajax({
        dataType: (DEV === 'release') ? 'json' : 'jsonp',
        url: "http://api.worldweatheronline.com/free/v1/weather.ashx?key=h8dtfkhry55kdzesyj9r58ph&num_of_days=3&q="+location+"&format=json",
        success: function (data) {
            var result = data.data.current_condition[0];
            var ourWeather = {
                precipitation: result.precipMM,
                windSpeed: result.windspeedKmph,
                hourOfDay: getHoursFromDate(result.observation_time),
                observationTime: result.observation_time,
                cloudcover : result.cloudcover,
                humidity : result.humidity,
                temp_C : result.temp_C,
                query: data.data.request[0].query,
                queryType: data.data.request[0].type,
                description: result.weatherDesc[0].value,
                icon: result.weatherIconUrl[0].value
            };
            for(var i in ourWeather){
                result[i] = ourWeather[i];
            }
            show_weather(result);
        },
        error: function(data){
            console.log('error getting api');
        }
    });
}

function getHoursFromDate(datestring){
    var hours12 = datestring.split(':')[0],
        result;
    if(datestring.indexOf('AM') != -1){
        result = parseInt(hours12);
        if(result == 12){
            result = 0;
        }
        return result;
    }else{
        var hours24 = hours12;
        if(hours12 != '12'){
            hours24 = parseInt(hours12) + 12.0;
        }
        result = parseInt(hours24);
        return result;
    }
}

function show_weather(weather){

    var html = '';

    html += '<img src="' + weather.weatherIconUrl[0].value + '"/>';
    if(weather.queryType.toLowerCase() == 'latlon'){
        html += '<p><strong>Current Location</strong></p>';
    }else{
        html += '<p><strong>' + weather.query + '</strong></p>';
    }
    html += '<p>' + weather.description + '</p>';
    html += '<p>Last updated: ' + weather.observationTime + '</p>';

    $('.current_weather').html(html);

    var goodClothes = check_clothing(weather);
    
    $.each(goodClothes, function(index,item){
        $('.good_clothes').append('<li>'+item.title+'</li>');

    });


}

var mylocation = '';

/*
navigator.geolocation.getCurrentPosition(function(){
    // mylocation = position.coords.latitude+','+position.coords.longitude;
     //get_weather(mylocation);
}, function(){
    // // var random = Math.floor(Math.random()*config.cities.length);
    // location = config.cities[random];
    // get_weather(mylocation);
})*/



var random = Math.floor(Math.random()*cities.length);
mylocation = cities[random];
get_weather(mylocation);


function check_clothing(current_weather_condition){
    var match = true;
    var badClothes = [];
//console.log(clothes);
    $.each(clothes, function(index, item){
//console.log(index+item.title);
         $.each(item.conditions, function(key, condition){    

            theirValue = parseFloat(current_weather_condition[condition.title]);
            ourValue = parseFloat(condition.value);

           

            switch(condition.operator){
                case '>':
                if(theirValue < ourValue){
                    match = false;
                }
                break;
                 case '<':
                if(theirValue > ourValue){
                    match = false;
                }
                break;
                 case '=':
                if(theirValue === ourValue){
                    match = false;
                }
                break;
            }

           if(match === false){
                //console.log('BAD '+index+' - '+item.title+' - '+condition.title);
                badClothes.push(index);
            }else{
                //console.log('Still ok - '+index+' - '+item.title+' - '+condition.title);
            }
        });
    });
    console.log(badClothes);

    $.each(badClothes, function(index, value){
        clothes.splice(value , 1);
    });

    return clothes;
}