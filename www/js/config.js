
var cities = [
        'bournemouth',
        'cardiff',
        'los angeles',
       'new delhi',
       'tokyo',
       'london',
      'edinburgh',
        'iceland',
        'melbourne',
        'rio de janiero',
       'istanbul',
       'glasgow',
       'oslo',
        'berlin',
      'vancouver',
      'paris',
      'rome',
      'truro',
        'las vegas'
    ];

var clothes = [
        {
            title: 'Umbrella',
            icon : '',
            important : true,
            conditions: [
                {
                    title: 'windSpeed',
                    operator: '<',
                    value: 30
                },
                {
                    title: 'precipitation',
                    operator: '>',
                    value: 0.2
                }
            ]
        },
        {
            title: 'Sunglasses',
              icon : '',
            important : false,
             conditions: [
                {
                    title: 'cloudcover',
                    operator: '<',
                    value: 15
                },
                {
                    title: 'hourOfDay',
                    operator: '>',
                    value: 9
                },
                {
                    title: 'hourOfDay',
                    operator: '<',
                    value: 19
                }

            ]
        },
        {
            title: 'Loose Clothing',
            icon : '',
            important : true,
             conditions: [
                {
                    title: 'humidity',
                    operator: '>',
                    value: 120
                },
                {
                    title: 'temp_C',
                    operator: '>',
                    value: 22
                }
            ]
        },
         {
            title: 'Open Footwear',
            icon : '',
            important : true,
             conditions: [
                {
                    title: 'temp_C',
                    operator: '>',
                    value: 21
                },
                {
                    title: 'precipitation',
                    operator: '<',
                    value: 0.9
                }

            ]
        },
        {
            title: 'Outer Layer',
            icon : '',
            important : true,
             conditions: [
                {
                    title: 'temp_C',
                    operator: '<',
                    value: 11
                }
            ]
        },
        {
            title: 'Warm Clothing',
             conditions: [
                {
                    title: 'temp_C',
                    operator: '<',
                    value: 16
                }
            ]
        },
        {
            title: 'Waterproofs',
              icon : '',
            important : true,
             conditions: [
                {
                    title: 'temp_C',
                    operator: '<',
                    value: 16
                },
                {
                    title: 'precipitation',
                    operator: '>',
                    value: 0.9
                }
            ]
        },
        {
            title: 'Large Headwear',
              icon : '',
            important : true,
             conditions: [
                {
                    title: 'temp_C',
                    operator: '>',
                    value: 23
                },
                {
                    title: 'windSpeed',
                    operator: '<',
                    value: 30
                },
                 {
                    title: 'cloudcover',
                    operator: '<',
                    value: 15
                },
            ]
        },
        {
            title: 'Skirts &amp; Dress',
              icon : '',
            important : false,
             conditions: [
                {
                    title: 'temp_C',
                    operator: '>',
                    value: 8
                },
                {
                    title: 'windSpeed',
                    operator: '<',
                    value: 30
                }
            ]
        },
        {
            title: 'Suncream',
              icon : '',
            important : true,
             conditions: [
                {
                    title: 'cloudcover',
                    operator: '<',
                    value: 15
                },
                {
                    title: 'hourOfDay',
                    operator: '>',
                    value: 10
                },
                {
                    title: 'hourOfDay',
                    operator: '<',
                    value: 19
                }
            ]
        }
    ]